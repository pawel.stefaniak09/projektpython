class myFunctions:
    st = []
    gnr = {}
    group = {}
    education = {}
    food = {}
    resultExam = {}
    mathScore = {}
    readScore = {}
    writeScore = {}
    komplet = []

    def __init__(self, dane):
        self.st = dane

    def gender(self):
        female = 0
        male = 0
        for i in range(len(self.st)):
            if self.st.loc[i, "gender"] == "female":
                female += 1
            else:
                male += 1
        self.gnr["male"] = male
        self.gnr["female"] = female
        self.komplet.append(male)
        self.komplet.append(female)

    def race(self):
        A = 0
        B = 0
        C = 0
        D = 0
        E = 0
        for i in range(len(self.st)):
            if self.st.loc[i, "race"] == "group A":
                A += 1
            elif self.st.loc[i, "race"] == "group B":
                B += 1
            elif self.st.loc[i, "race"] == "group C":
                C += 1
            elif self.st.loc[i, "race"] == "group D":
                D += 1
            else:
                E += 1
        self.group["A"] = A
        self.group["B"] = B
        self.group["C"] = C
        self.group["D"] = D
        self.group["E"] = E
        self.komplet.append(A)
        self.komplet.append(B)
        self.komplet.append(C)
        self.komplet.append(D)
        self.komplet.append(E)

    def levelEducation(self):
        bachelor = 0
        some = 0
        master = 0
        high = 0
        associate = 0
        someh = 0
        for i in range(len(self.st)):
            if self.st.loc[i, "parental level of education"] == "bachelor's degree":
                bachelor += 1
            elif self.st.loc[i, "parental level of education"] == "some college":
                some += 1
            elif self.st.loc[i, "parental level of education"] == "master's degree":
                master += 1
            elif self.st.loc[i, "parental level of education"] == "associate's degree":
                associate += 1
            elif self.st.loc[i, "parental level of education"] == "high school":
                high += 1
            else:
                someh += 1
        self.education["bachelor's degree"] = bachelor
        self.education["some college"] = master
        self.education["master's degree"] = some
        self.education["associate's degree"] = associate
        self.education["high school"] = high
        self.education["some high school"] = someh
        self.komplet.append(bachelor)
        self.komplet.append(master)
        self.komplet.append(some)
        self.komplet.append(associate)
        self.komplet.append(high)
        self.komplet.append(someh)

    def lunch(self):
        standard = 0
        free = 0
        for i in range(len(self.st)):
            if self.st.loc[i, "lunch"] == "standard":
                standard += 1
            else:
                free += 1
        self.food["standard"] = standard
        self.food["free/reduced"] = free
        self.komplet.append(standard)
        self.komplet.append(free)

    def result(self):
        complete = 0
        none = 0
        for i in range(len(self.st)):
            if self.st.loc[i, "test preparation course"] == "completed":
                complete += 1
            else:
                none += 1
        self.resultExam["completed"] = complete
        self.resultExam["none"] = none
        self.komplet.append(complete)
        self.komplet.append(none)

    def math(self):
        suma = 0
        max = 0
        min = 0
        for i in range(len(self.st)):
            if self.st.loc[i, "math score"] > max:
                max = self.st.loc[i, "math score"]
            if self.st.loc[i, "math score"] < min:
                min = self.st.loc[i, "math score"]
            suma += self.st.loc[i, "math score"]
        srednia = suma / len(self.st)
        self.mathScore["srednia"] = srednia
        self.mathScore["max"] = max
        self.mathScore["min"] = min
        self.komplet.append(srednia)
        self.komplet.append(max)
        self.komplet.append(min)

    def read(self):
        suma = 0
        max = 0
        min = 0
        for i in range(len(self.st)):
            if self.st.loc[i, "reading score"] > max:
                max = self.st.loc[i, "reading score"]
            if self.st.loc[i, "reading score"] < min:
                min = self.st.loc[i, "reading score"]
            suma += self.st.loc[i, "reading score"]
        srednia = suma / len(self.st)
        self.readScore["srednia"] = srednia
        self.readScore["max"] = max
        self.readScore["min"] = min
        self.komplet.append(srednia)
        self.komplet.append(max)
        self.komplet.append(min)

    def write(self):
        max = 0
        min = 0
        suma = 0
        for i in range(len(self.st)):
            if self.st.loc[i, "writing score"] > max:
                max = self.st.loc[i, "writing score"]
            if self.st.loc[i, "writing score"] < min:
                min = self.st.loc[i, "writing score"]
            suma += self.st.loc[i, "writing score"]
        srednia = suma / len(self.st)
        self.writeScore["srednia"] = srednia
        self.writeScore["max"] = max
        self.writeScore["min"] = min
        self.komplet.append(srednia)
        self.komplet.append(max)
        self.komplet.append(min)

    def wypisz(self):
        self.gender()
        print("Ilosc wystapien female: ", self.gnr["female"])
        print("Ilosc wystapien male: ", self.gnr["male"])
        self.race()
        print("Ilosc wystapiec grupy A: ", self.group["A"])
        print("Ilosc wystapiec grupy B: ", self.group["B"])
        print("Ilosc wystapiec grupy C: ", self.group["C"])
        print("Ilosc wystapiec grupy D: ", self.group["D"])
        print("Ilosc wystapiec grupy E: ", self.group["E"])
        self.levelEducation()
        print("Ilosc wystapien bachelor's degree: ", self.education["bachelor's degree"])
        print("Ilosc wystapien master's degree: ", self.education["master's degree"])
        print("Ilosc wystapien some college: ", self.education["some college"])
        print("Ilosc wystapien associate's degree: ", self.education["associate's degree"])
        print("Ilosc wystapien high school: ", self.education["high school"])
        print("Ilosc wystapien some high school: ", self.education["some high school"])
        self.lunch()
        print("Ilosc wystapien standard lunch: ", self.food["standard"])
        print("Ilosc wystapien free/reduced lunch: ", self.food["free/reduced"])
        self.result()
        print("Ilosc zdanych egzaminow: ", self.resultExam["completed"])
        print("Ilosc niezdanych egazaminow: ", self.resultExam["none"])
        self.math()
        print("Srednia wartosc MathScore: ", self.mathScore["srednia"])
        print("Maksymalna wartosc MathScore: ", self.mathScore["max"])
        print("Minimalna wartosc MathScore: ", self.mathScore["min"])
        self.read()
        print("Srednia wartosc ReadScore: ", self.readScore["srednia"])
        print("Maksymalna wartosc ReadScore: ", self.readScore["max"])
        print("Minimalna wartosc ReadScore: ", self.readScore["min"])
        self.write()
        print("Srednia wartosc WriteScore: ", self.writeScore["srednia"])
        print("Maksymalna wartosc WriteScore: ", self.writeScore["max"])
        print("Minimalna wartosc WriteScore: ", self.writeScore["min"])


class myModifies:
    st = []
    zbiorDanych = {}
    zbiorDanychAsoc = []

    def __init__(self, dane):
        self.st = dane

    def modyfikacje(self):
        self.st.loc[self.st["gender"] == "male", "gender"] = 1
        self.st.loc[self.st["gender"] == "female", "gender"] = 2

        self.st.loc[self.st["race"] == "group A", "race"] = 1
        self.st.loc[self.st["race"] == "group B", "race"] = 2
        self.st.loc[self.st["race"] == "group C", "race"] = 3
        self.st.loc[self.st["race"] == "group D", "race"] = 4
        self.st.loc[self.st["race"] == "group E", "race"] = 5

        self.st.loc[self.st["parental level of education"] == "bachelor's degree", "parental level of education"] = 1
        self.st.loc[self.st["parental level of education"] == "some college", "parental level of education"] = 2
        self.st.loc[self.st["parental level of education"] == "master's degree", "parental level of education"] = 3
        self.st.loc[self.st["parental level of education"] == "associate's degree", "parental level of education"] = 4
        self.st.loc[self.st["parental level of education"] == "some high school", "parental level of education"] = 5
        self.st.loc[self.st["parental level of education"] == "high school", "parental level of education"] = 6

        self.st.loc[self.st["lunch"] == "standard", "lunch"] = 1
        self.st.loc[self.st["lunch"] == "free/reduced", "lunch"] = 2
        return self.st

    @staticmethod
    def zmiana(dane):
        for i in range(len(dane)):
            if dane[i] == "none":
                dane[i] = 0
            else:
                dane[i] = 1

    @staticmethod
    def zmianav2(dane):
        for i in range(len(dane)):
            if dane[i] == 1:
                dane[i] = "completed"
            else:
                dane[i] = "none"

    def lista(self, dane, klasyfikatory):
        for i in range(len(dane)):
            self.zbiorDanych[dane[i]] = klasyfikatory[i]

    def najlepszy(self, dane, klasyfikatory):
        self.lista(dane, klasyfikatory)
        zbior = list(self.zbiorDanych)
        zbior.sort()
        zbior.reverse()
        max = zbior[0]
        for k in zbior:
            if k == max:
                print("Najlepszy klasyfikator to: ", self.zbiorDanych[k], "ktorego dokladnosc wynosi: ", k)

    @staticmethod
    def modAsocjacja(dane):
        tablica = dane
        math = []
        write = []
        read = []
        tab = {}
        for i in range(len(dane)):
            if dane[i][0] >= 65:
                tablica[i][0] = 1
                math.append(1)
            else:
                tablica[i][0] = 0
                math.append(0)
            if dane[i][1] >= 65:
                tablica[i][1] = 1
                write.append(1)
            else:
                tablica[i][1] = 0
                write.append(0)
            if dane[i][2] >= 65:
                tablica[i][2] = 1
                read.append(1)
            else:
                tablica[i][2] = 0
                read.append(0)
        tab["math score"] = math
        tab["read score"] = read
        tab["writing score"] = write
        return tab
