###### Projekt ######
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier, VotingClassifier, BaggingClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import normalize
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn import tree
from keras.models import Sequential
from keras.layers import Dense, Dropout
from sklearn.tree import DecisionTreeClassifier
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.preprocessing import OneHotEncoder
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.metrics import plot_confusion_matrix
from sklearn import metrics
from sklearn import model_selection
from mlxtend.frequent_patterns import apriori, association_rules
import seaborn as sns
import matplotlib.pyplot as plt
### Moje Metody

from mojKlasyfikator import myQualifier
from mojeMetody import myModifies, myFunctions

print(" ")

### Pobieranie tabeli csv
st = pd.read_csv('StudentsPerformance.csv')
print("Liczba pustych pól wynosi: ", st.isnull().sum().sum())

print(st.describe())
print(st.info())

### Podsumowanie kazdej kolumny danych
func = myFunctions(st)
func.wypisz()

## Wykres podsumowania kazdej kolumny
height = func.komplet
bars = ('male', 'female', 'A', 'B', 'C', 'D', 'E',
        "bachelor", 'college', "master", "associate'", 'high', 'some high',
        'standard', 'free', 'completed', 'none', 'M-sr', 'M-max', 'M-min',
        'R-sr', 'R-max', 'R-min', 'W-sr', 'W-max', 'W-min')
y_pos = np.arange(len(height))
plt.figure(figsize=(20, 10))
plt.bar(y_pos, height, color='#000099')
plt.xticks(y_pos, bars)
plt.xlabel('Wartości bazy', fontsize=18, color='#323232')
plt.ylabel('Ilość wystąpień', fontsize=18, color='#323232')
plt.title('Wykres wystąpień danych w bazie', fontsize=20, color='#323232')
# plt.show();

### Modyfikacja kolumn na wartosci int
mod = myModifies(st)
st = mod.modyfikacje()

### Losowe ustawienie rekordow tablicy
# st = st.iloc[np.random.permutation(len(st))]

### Dzielenie bazy danych na zbiór testowy i treningowy
all_inputs = st[['gender', 'race', 'parental level of education', 'lunch', 'math score', 'reading score', 'writing score']].values
all_classes = st['test preparation course'].values

### Normalizacja tabel wejsciowych oraz dzielenie
X_normalized = normalize(all_inputs, axis=0)
(train_inputs, test_inputs, train_classes, test_classes) = train_test_split(X_normalized, all_classes, train_size=0.65, random_state=1)

############### Naive Bayes ###############
print(" ")
print("Klasyfikator Naive Bayes :")

gnb = GaussianNB()
y_pred = gnb.fit(test_inputs, test_classes).predict(test_inputs)
print("Liczba błędnie oznaczonych punktów z łącznej liczby %d punktów: %d" % (test_inputs.shape[0], (test_classes != y_pred).sum()))
procentNB = (test_inputs.shape[0]-(test_classes != y_pred).sum())/test_inputs.shape[0]*100
print("Dokladnosc klasyfikatora Naive Bayes wynosi: ", procentNB)
wykres = []
wykres.append(procentNB)
plot_confusion_matrix(gnb, test_inputs, test_classes)
plt.title("Naive Bayes")

############### K-NN (3-sasiadow) ###############
print(" ")
print("Klasyfikator K-NN (3):")

knn = KNeighborsClassifier(n_neighbors=3, metric='euclidean')
y_pred = knn.fit(test_inputs, test_classes).predict(test_inputs)
print("Liczba błędnie oznaczonych punktów z łącznej liczby %d punktów: %d" % (test_inputs.shape[0], (test_classes != y_pred).sum()))
procentKNN3 = (test_inputs.shape[0]-(test_classes != y_pred).sum())/test_inputs.shape[0]*100
print("Dokladnosc klasyfikatora K-NN dla 3 sąsiadów wynosi: ", procentKNN3)
wykres.append(procentKNN3)
plot_confusion_matrix(knn, test_inputs, test_classes)
plt.title("K-NN (3)")

############### K-NN (5-sasiadow) ###############
print(" ")
print("Klasyfikator K-NN (5):")

knn = KNeighborsClassifier(n_neighbors=5, metric='euclidean')
y_pred = knn.fit(test_inputs, test_classes).predict(test_inputs)
print("Liczba błędnie oznaczonych punktów z łącznej liczby %d punktów: %d" % (test_inputs.shape[0], (test_classes != y_pred).sum()))
procentKNN5 = (test_inputs.shape[0]-(test_classes != y_pred).sum())/test_inputs.shape[0]*100
print("Dokladnosc klasyfikatora K-NN dla 4 sąsiadów wynosi: ", procentKNN5)
wykres.append(procentKNN5)
plot_confusion_matrix(knn, test_inputs, test_classes)
plt.title("K-NN (5)")

############### K-NN (9-sasiadow) ###############
print(" ")
print("Klasyfikator K-NN (9):")

knn = KNeighborsClassifier(n_neighbors=9, metric='euclidean')
y_pred = knn.fit(test_inputs, test_classes).predict(test_inputs)
print("Liczba błędnie oznaczonych punktów z łącznej liczby %d punktów: %d" % (test_inputs.shape[0], (test_classes != y_pred).sum()))
procentKNN9 = (test_inputs.shape[0]-(test_classes != y_pred).sum())/test_inputs.shape[0]*100
print("Dokladnosc klasyfikatora K-NN dla 9 sąsiadów wynosi: ", procentKNN9)
wykres.append(procentKNN9)
plot_confusion_matrix(knn, test_inputs, test_classes)
plt.title("K-NN (9)")

############### K-NN (13-sasiadow) ###############
print(" ")
print("Klasyfikator K-NN (13):")

knn = KNeighborsClassifier(n_neighbors=13, metric='euclidean')
y_pred = knn.fit(test_inputs, test_classes).predict(test_inputs)
print("Liczba błędnie oznaczonych punktów z łącznej liczby %d punktów: %d" % (test_inputs.shape[0], (test_classes != y_pred).sum()))
procentKNN13 = (test_inputs.shape[0]-(test_classes != y_pred).sum())/test_inputs.shape[0]*100
print("Dokladnosc klasyfikatora K-NN dla 13 sąsiadów wynosi: ", procentKNN13)
wykres.append(procentKNN13)
plot_confusion_matrix(knn, test_inputs, test_classes)
plt.title("K-NN (13)")
plt.show()

############### Drzewo decyzyjne ###############
print(" ")
print("Klasyfikator drzewo decyzyjne:")

dtc = tree.DecisionTreeClassifier()
y_pred = dtc.fit(test_inputs, test_classes).predict(test_inputs)
print("Liczba błędnie oznaczonych punktów z łącznej liczby %d punktów: %d" % (test_inputs.shape[0], (test_classes != y_pred).sum()))
procentT = (dtc.score(test_inputs, test_classes)*100)
print("Dokladnosc klasyfikatora drzewa decyzyjnego wynosi: ", procentT)
wykres.append(procentT)
tree.plot_tree(dtc)
plot_confusion_matrix(dtc, test_inputs, test_classes)
plt.title("Drzewo decyzyjne (Tree)")

############### Sieci neuronowe ###############
### Przygotowanie tabel do pracy
mod.zmiana(test_classes)
y_ = test_classes.reshape(-1, 1)
encoder = OneHotEncoder(sparse=False)
test_classes = encoder.fit_transform(y_)
mod.zmiana(train_classes)
y_ = train_classes.reshape(-1, 1)
encoder = OneHotEncoder(sparse=False)
train_classes = encoder.fit_transform(y_)

############### Sieci neuronowe V1 ###############
print(" ")
print("Klasyfikator sieci neuronowe V1:")

### nauczanie sieci
modelv1 = Sequential()
modelv1.add(Dense(7, input_dim=7, activation='relu'))
modelv1.add(Dense(7, activation='relu'))
modelv1.add(Dense(2, activation='softmax'))
modelv1.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
# modelv1.summary()
history = modelv1.fit(test_inputs, test_classes, validation_data=(test_inputs, test_classes), batch_size=14, epochs=10, verbose=0)

### Rysowanie wykresu
# fig, ax = plt.subplots()
# ax.plot(range(1,11), history.history['accuracy'], label='Test Accuracy')
# ax.plot(range(1,11), history.history['val_accuracy'], label='Validation Accuracy')
# ax.legend(loc='best')
# ax.set(xlabel='epochs', ylabel='accuracy')
# plt.show()

### Wyciaganie wyniku
prediction=modelv1.predict(test_inputs)
length=len(prediction)
y_label=np.argmax(test_classes,axis=1)
predict_label=np.argmax(prediction,axis=1)
procentSCV1 = np.sum(y_label==predict_label)/length * 100
wykres.append(procentSCV1)
print("Dokladnosc klasyfikatora sieci neuronowej v1 wynosi: ", procentSCV1)

############### Sieci neuronowe V2 ###############
print(" ")
print("Klasyfikator sieci neuronowe V2:")

### nauczanie sieci
modelv2 = Sequential()
modelv2.add(Dense(128, input_dim=7, activation='sigmoid'))
modelv2.add(Dropout(0.3))
modelv2.add(Dense(64, activation='sigmoid'))
modelv2.add(Dropout(0.3))
modelv2.add(Dense(32, activation='sigmoid'))
modelv2.add(Dropout(0.3))
modelv2.add(Dense(2, activation='softmax'))
modelv2.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
# modelv2.summary()

EarlyStop = EarlyStopping(monitor='val_loss',
                          patience=3,
                          verbose=1)

modelv2.fit(test_inputs, test_classes, validation_data=(test_inputs, test_classes), batch_size=1024, epochs=50, verbose=0, callbacks=[EarlyStop])

prediction=modelv2.predict(test_inputs)
length=len(prediction)
y_label=np.argmax(test_classes,axis=1)
predict_label=np.argmax(prediction,axis=1)
procentSCV2 = np.sum(y_label==predict_label)/length * 100
wykres.append(procentSCV2)
print("Dokladnosc klasyfikatora sieci neuronowej v2 wynosi: ", procentSCV2)

############### Wyjscie z kwalifikatorow sieci neuronowej ###############
### Modyfikacja zmiennych do oryginalnej formy
yte = encoder.inverse_transform(test_classes)
test_classes = np.reshape(yte, 350)
mod.zmianav2(test_classes)

ytr = encoder.inverse_transform(train_classes)
train_classes = np.reshape(ytr, 650)
mod.zmianav2(train_classes)

############### Support vector machines (SVMs) ###############
print(" ")
print("Support vector machines (SVMs):")

clf = svm.SVC(decision_function_shape='ovo')
# clf = svm.SVC(kernel='linear')
y_pred = clf.fit(test_inputs, test_classes).predict(test_inputs)
print("Liczba błędnie oznaczonych punktów z łącznej liczby %d punktów: %d" % (test_inputs.shape[0], (test_classes != y_pred).sum()))
procentSVM = (metrics.accuracy_score(test_classes, y_pred) * 100)
wykres.append(procentSVM)
print("Dokladnosc klasyfikatora SVMs wynosi: ", procentSVM)
plot_confusion_matrix(clf, test_inputs, test_classes)
plt.title("SVMS")
# plt.show()

############### RandomForestClassifier ###############
print(" ")
print("RandomForestClassifier")

rfc=RandomForestClassifier(n_estimators=100)
y_pred = rfc.fit(test_inputs, test_classes).predict(test_inputs)
print("Liczba błędnie oznaczonych punktów z łącznej liczby %d punktów: %d" % (test_inputs.shape[0], (test_classes != y_pred).sum()))
procentRFC = (metrics.accuracy_score(test_classes, y_pred) * 100)
wykres.append(procentRFC)
print("Dokladnosc klasyfikatora SVMs wynosi: ", procentRFC)
plot_confusion_matrix(rfc, test_inputs, test_classes)
plt.title("RandomForest")

############### LogisticRegression ###############
print(" ")
print("Regresja logistyczna")

lacc = 0
lr = LogisticRegression(solver='lbfgs')
y_pred = lr.fit(test_inputs, test_classes).predict(test_inputs)
print("Liczba błędnie oznaczonych punktów z łącznej liczby %d punktów: %d" % (test_inputs.shape[0], (test_classes != y_pred).sum()))
lacc += (accuracy_score(y_pred, test_classes) * 100)
wykres.append(lacc)
print("Dokladnosc klasyfikatora SVMs wynosi: ", lacc)
plot_confusion_matrix(lr, test_inputs, test_classes)
plt.title("Regresja logistyczna")

############### Metody Ensemble ###############
print(" ")
print("Metoda pakowania")
print("Drzewa decyzyjne w workach")
tree = model_selection.KFold(n_splits=10, random_state=7, shuffle=True)
cart = DecisionTreeClassifier()
modelT = BaggingClassifier(base_estimator=cart, n_estimators=128, random_state=7)
resultsT = (model_selection.cross_val_score(modelT, test_inputs, test_classes, cv=tree) * 100)
print("Dokladnosc prognozy metody pakowania wynosi: ", resultsT.mean())
wykres.append(resultsT.mean())

print(" ")
print("Metoda Wzmacniania")
print("Wzmocnienie gradientu")
grad = model_selection.KFold(n_splits=10, random_state=7, shuffle=True)
modelG = GradientBoostingClassifier(n_estimators=128, random_state=7)
resultsGr = (model_selection.cross_val_score(modelG, test_inputs, test_classes, cv=grad) * 100)
print("Dokladnosc prognozy metody wzmacniania wynosi: ", resultsGr.mean())
wykres.append(resultsGr.mean())

print(" ")
print("Metoda Głosowania")
glos = model_selection.KFold(n_splits=10, random_state=7, shuffle=True)
# create the sub models
estimators = []
model1 = LogisticRegression()
estimators.append(('logistic', model1))
model2 = DecisionTreeClassifier()
estimators.append(('cart', model2))
model3 = SVC()
estimators.append(('svm', model3))
# create the ensemble model
ensemble = VotingClassifier(estimators)
resultsGl = (model_selection.cross_val_score(ensemble, test_inputs, test_classes, cv=glos) * 100)
print("Dokladnosc prognozy metody glosowania wynosi: ", resultsGl.mean())
wykres.append(resultsGl.mean())

############### Moj kwalifikator ###############
print(" ")
print("Moj wlasny kwalifikator")

mwq = myQualifier(test_inputs[:, 4:7], test_classes)
mq = mwq.fit()
wykres.append(mq)

############### Reguly asocjacyjne ###############
print(" ")
print("Reguly asocjacyjne")

tablica = mod.modAsocjacja(st[["math score", "reading score", "writing score"]].values)

lista = pd.DataFrame(tablica)

freq_items = apriori(lista, min_support=0.2, use_colnames=True)

rules = association_rules(freq_items, metric="confidence", min_threshold=0.55)
# print(rules.head())
print(rules["confidence"])
rules.sort_values(by='confidence', ascending=False).head()
rules["rule"] = rules["antecedents"].apply(lambda x: ', '.join(list(x))).astype("unicode") + \
        '->' + rules["consequents"].apply(lambda x: ', '.join(list(x))).astype("unicode")
import seaborn as sns
import matplotlib.pyplot as plt
sns.set_context('paper')
sns.catplot(data=rules,
           x = "confidence",
           y = "lift",
           aspect = 1.5,
           hue = "rule")

############### Podsumowanie ###############
height = wykres
bars = ('NaiveB', 'K-NN(3)', 'K-NN(5)', 'K-NN(9)', 'K-NN(13)',
        "Tree", "Siec V1", "Siec V2", "SVMs", "RadnomF",
        "Regresja","Pak","Wzmac","Glos", "MyClasifier")
y_pos = np.arange(len(height))
plt.figure(figsize=(10, 5))
plt.bar(y_pos, height, color='#000099')
plt.xticks(y_pos, bars)
plt.xlabel('Rodzaj klasyfikatora', fontsize=12, color='#323232')
plt.ylabel('Dopasowanie [%]', fontsize=12, color='#323232')
plt.title('Wykres dopasowania klasyfikatorow', fontsize=16, color='#323232')
plt.show();

### Najlepszy klasyfikator
print(" ")
mod.najlepszy(wykres, bars)


