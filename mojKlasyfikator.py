class myQualifier:
    sredniaWartoscWyniku = 0
    iloscZdanych = 0
    iloscEgzaminow = 0
    egzaminy = []
    wynikiEgzaminow = []
    dokladnosc = 0
    blad = 0
    def __init__(self, testy, wyniki):
        self.egzaminy = testy
        self.wynikiEgzaminow = wyniki
        self.iloscEgzaminow = len(testy)
    def srednia(self):
        pasExam = 0
        for i in range(self.iloscEgzaminow):
            suma = self.egzaminy[i, 0] + self.egzaminy[i, 1] + self.egzaminy[i, 2]
            if self.wynikiEgzaminow[i] == 'completed':
                self.iloscZdanych += 1
                pasExam += suma
        self.sredniaWartoscWyniku = pasExam / self.iloscZdanych
    def wyniki(self):
        wynik = 0
        for i in range(len(self.egzaminy)):
            suma = self.egzaminy[i, 0] + self.egzaminy[i, 1] + self.egzaminy[i, 2]
            if suma > self.sredniaWartoscWyniku:
                wynik += 1
        if wynik > self.iloscZdanych:
            self.dokladnosc = (self.iloscZdanych / wynik) * 100
            self.blad = wynik - self.iloscZdanych
        else:
            self.dokladnosc = (wynik / self.iloscZdanych) * 100
            self.blad = self.iloscZdanych - wynik
    def wypisz(self):
        # print("Srednia: ", self.sredniaWartoscWyniku)
        # print("Ilosc Zdanych Egzaminow: ", self.iloscZdanych)
        # print("Ilosc Egzaminow: ", self.iloscEgzaminow)
        # print("Egzaminy: ", self.egzaminy)
        # print("Wyniki Egzaminow: ", self.wynikiEgzaminow)
        print("Liczba błędnie oznaczonych punktów z łącznej liczby 350 punktów: ", self.blad)
        print("Dokladnosc mojego wlasnego klasyfikatora wynosi: ", self.dokladnosc)
    def fit(self):
        self.srednia()
        self.wyniki()
        self.wypisz()
        return self.dokladnosc